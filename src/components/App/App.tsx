import React from 'react';
import { Switch, Route } from 'react-router';
import TodoList from '../TodoList/TodoList';
import { Navbar } from '../Navbar/Navbar';
import { NewList } from '../TodoList/NewList';

const App = function () {
  return (
    <div>
      <Navbar />
      <Switch>
        <Route exact path="/">
          <TodoList />
        </Route>
        <Route path="/newlist">
          <NewList />
        </Route>
      </Switch>
    </div>
  );
};

export default App;
