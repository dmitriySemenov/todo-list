import {
  CREATE_TODO,
  DELETE_TODO,
  EDIT_TODO,
  SELECT_TODO,
  TOGGLE_TODO,
} from '../constants/todo-constants';

export interface Todo {
  id: string;
  desc: string;
  isComplete: boolean;
}

export interface State {
  todos: Todo[];
  selectedTodo: string | null;
  counter: number;
}

export interface CreateTodoActionType {
  type: typeof CREATE_TODO;
  payload: Todo;
}

export interface EditTodoActionType {
  type: typeof EDIT_TODO;
  payload: { id: string; desc: string };
}

export interface ToggleTodoActionType {
  type: typeof TOGGLE_TODO;
  payload: { id: string; isComplete: boolean };
}

export interface DeleteTodoActionType {
  type: typeof DELETE_TODO;
  payload: { id: string };
}

export interface SelectTodoActionType {
  type: typeof SELECT_TODO;
  payload: { id: string };
}
