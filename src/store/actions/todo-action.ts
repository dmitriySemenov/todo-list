import {
  CREATE_TODO,
  DELETE_TODO,
  EDIT_TODO,
  SELECT_TODO,
  TOGGLE_TODO,
} from '../../constants/todo-constants';
import { v1 as uuid } from 'uuid';

import {
  CreateTodoActionType,
  EditTodoActionType,
  ToggleTodoActionType,
  DeleteTodoActionType,
  SelectTodoActionType,
} from '../../typings/types';

export const createTodoActionCreator = (data: {
  desc: string;
}): CreateTodoActionType => {
  const { desc } = data;
  return {
    type: CREATE_TODO,
    payload: {
      id: uuid(),
      desc,
      isComplete: false,
    },
  };
};

export const editTodoActionCreator = (data: {
  id: string;
  desc: string;
}): EditTodoActionType => {
  const { id, desc } = data;
  return {
    type: EDIT_TODO,
    payload: {
      id,
      desc,
    },
  };
};

export const toggleTodoActionCreator = (data: {
  id: string;
  isComplete: boolean;
}): ToggleTodoActionType => {
  const { id, isComplete } = data;
  return {
    type: TOGGLE_TODO,
    payload: {
      id,
      isComplete,
    },
  };
};

export const deleteTodoActionCreator = (data: {
  id: string;
}): DeleteTodoActionType => {
  const { id } = data;
  return {
    type: DELETE_TODO,
    payload: {
      id,
    },
  };
};

export const selectTodoActionCreator = (data: {
  id: string;
}): SelectTodoActionType => {
  const { id } = data;
  return {
    type: SELECT_TODO,
    payload: {
      id,
    },
  };
};
