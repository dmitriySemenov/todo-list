import { Todo } from '../../typings/types';
import { v1 as uuid } from 'uuid';
import { combineReducers, createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import {
  CREATE_TODO,
  EDIT_TODO,
  TOGGLE_TODO,
  DELETE_TODO,
  SELECT_TODO,
} from '../../constants/todo-constants';
import {
  CreateTodoActionType,
  EditTodoActionType,
  ToggleTodoActionType,
  SelectTodoActionType,
  DeleteTodoActionType,
} from '../../typings/types';

const todosInitialState: Todo[] = [
  {
    id: uuid(),
    desc: 'Learn React',
    isComplete: true,
  },
  {
    id: uuid(),
    desc: 'Learn Redux',
    isComplete: true,
  },
  {
    id: uuid(),
    desc: 'Learn Redux-ToolKit',
    isComplete: false,
  },
];

//Reducers
type TodoActionTypes =
  | CreateTodoActionType
  | EditTodoActionType
  | ToggleTodoActionType
  | DeleteTodoActionType;

export const todosReducer = (
  state: Todo[] = todosInitialState,
  action: TodoActionTypes,
) => {
  switch (action.type) {
    case CREATE_TODO: {
      return [...state, action.payload];
    }
    case EDIT_TODO: {
      return state.map((todo) =>
        todo.id === action.payload.id
          ? { ...todo, desc: action.payload.desc }
          : todo,
      );
    }
    case TOGGLE_TODO: {
      return state.map((todo) =>
        todo.id === action.payload.id
          ? { ...todo, isComplete: action.payload.isComplete }
          : todo,
      );
    }
    case DELETE_TODO: {
      return state.filter((todo) => todo.id !== action.payload.id);
    }
    default: {
      return state;
    }
  }
};

type SelectedTodoActionTypes = SelectTodoActionType;

const selectedTodoReducer = (
  state: string | null = null,
  action: SelectedTodoActionTypes,
) => {
  switch (action.type) {
    case SELECT_TODO: {
      return action.payload.id;
    }
    default: {
      return state;
    }
  }
};

const counterReducer = (state = 0, action: TodoActionTypes) => {
  switch (action.type) {
    case CREATE_TODO: {
      return state + 1;
    }
    case EDIT_TODO: {
      return state + 1;
    }
    case TOGGLE_TODO: {
      return state + 1;
    }
    case DELETE_TODO: {
      return state + 1;
    }
    default: {
      return state;
    }
  }
};

const reducers = combineReducers({
  todos: todosReducer,
  selectedTodo: selectedTodoReducer,
  counter: counterReducer,
});

// Store
export default createStore(
  reducers,
  composeWithDevTools(applyMiddleware(thunk, logger)),
);
